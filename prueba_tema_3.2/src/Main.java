public class Main {
    public static void main(String[] args) {

        Coche micoche = new Coche();
        micoche.incremetar();
        micoche.incremetar();
        micoche.incremetar();
        micoche.incremetar();
        micoche.incremetar();
        System.out.println(micoche.num_puerta);
    }
}

class Coche{
    int num_puerta;

    public void incremetar(){
        num_puerta ++;
    }

}